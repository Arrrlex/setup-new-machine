#!/usr/bin/env python

"""
Script to (partially) automate setting up a new linux or WSL machine.

Note: python 3.8 is required, because I love the walrus operator.
"""

from pathlib import Path
import json
import subprocess
import sys
import collections

def is_success(cmd):
    return subprocess.run(f'zsh -ic "{cmd}"', shell=True).returncode == 0

def ask(question):
    print(question)
    answer = input('[yes|no] ')
    if answer in ['yes', 'y']:
        return True
    if answer in ['no', 'n']:
        return False
    else:
        print('Please type yes or no.')
        ask(question)

class CheckpointItemBase:
    def __init__(self, name, checkpoint):
        self.name = name
        self.checkpoint = checkpoint
        self.pull()

    def pull(self):
        if self.name in self.checkpoint.dict:
            self.__dict__.update(self.checkpoint.dict[self.name])
        else:
            self.set_defaults()

    def push(self):
        to_write = {k: self.__dict__[k] for k in self.attrs}
        self.checkpoint.write(self.name, to_write)

    def __setattr__(self, name, value):
        super().__setattr__(name, value)
        if name in self.attrs:
            self.push()


class InstallData(CheckpointItemBase):
    attrs = ['attempted', 'installed']

    def set_defaults(self):
        self.__dict__.update({k: False for k in self.attrs})

class Settings(CheckpointItemBase):
    attrs = ['os']
    options = {'os': ['ubuntu', 'other_linux', 'wsl']}

    def __init__(self, checkpoint):
        super().__init__(name='settings', checkpoint=checkpoint)

    def set_defaults(self):
        for (setting, options) in self.options.items():
            print(f"Set value {setting} from {', '.join(options)}:")
            while True:
                choice = input(f'[{options[0]}] ')
                if choice == '':
                    choice = options[0]
                if chosen_option := next((o for o in options if o.startswith(choice)), None):
                    break
                print("Please choose from {', '.join(options)}")


            self.__setattr__(setting, chosen_option)


class Checkpoint:
    def __init__(self):
        self.path = Path(__file__).resolve().parent / '.setup-new-machine.checkpoint'
        if self.path.exists():
            with self.path.open() as f:
                self.dict = json.load(f)
        else:
            self.dict = {}

        self.settings = Settings(self)
        
    
    def item(self, name):
        return InstallData(name, checkpoint=self)

    def write(self, key, value):
        self.dict[key] = value
        with self.path.open('w') as f:
            json.dump(self.dict, f)

    def install(self, name, test_cmd, install_cmd):
        item = self.item(name)
        if item.installed:
            return

        if test_cmd is None and ask(f'Is {name} already installed?'):
            item.installed = True
            return
        elif is_success(test_cmd):
            print(f'{name} is already installed.')
            item.installed = True
            return

        if not item.attempted:
            if isinstance(install_cmd, str):
                cmd = install_cmd
            elif isinstance(install_cmd, dict):
                cmd = install_cmd.get(self.settings.os)
                if cmd.startswith('@'):
                    key = cmd[1:]
                    cmd = install_cmd[key]

            if cmd is None:
                print(f'Please manually install {name} and restart shell.')
                sys.exit(1)

            print(f'Installing {name}')
            print('The following command will be run:')
            print(cmd)
            print('Press enter to continue: ', sep='')
            input()
            print()

            subprocess.run(cmd, shell=True)
            item.attempted = True
            print()
            print(f'{name} has (hopefully) been installed.')
            print('Restart shell now.')
            sys.exit(1)

        if test_cmd is None:
            if ask(f'Have you manually verified {name} is installed?'):
                return
            else:
                print(f'Please verify {name} is successfully installed and restart shell.')
                sys.exit(1)

        print(f'Verifying {name} install')
        if not is_success(test_cmd):
            print(f'Verifying {name} install failed. Please manually install and restart shell.')
            sys.exit(1)
        item.installed = True
        print(f'{name} successfully installed.')

if __name__ == '__main__':
    checkpoint = Checkpoint()
    checkpoint.install(name='pyenv',
            test_cmd='pyenv --version',
            install_cmd={
                'ubuntu': (r'sudo apt-get install -y build-essential libssl-dev zlib1g-dev libbz2-dev '
                    'libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev '
                    'xz-utils tk-dev libffi-dev liblzma-dev python-openssl git &&'
                    'curl https://pyenv.run | bash'),
                'wsl': '@ubuntu',
                'other_linux': None,
                }
            )
    
    checkpoint.install(name='python 3.8.5',
            test_cmd=None,
            install_cmd='pyenv install 3.8.5 && pyenv global 3.8.5',
            )

    checkpoint.install(name='brew',
            test_cmd='brew --version',
            install_cmd=r'/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"',
            )

    checkpoint.install(name='pipx',
            test_cmd='pipx --version',
            install_cmd=r'pip install --user pipx',
            )

    checkpoint.install(name='poetry',
            test_cmd='poetry --version',
            install_cmd=r'pipx install poetry',
            )

    checkpoint.install(name='oh-my-zsh',
            test_cmd=None,
            install_cmd=r'sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"'
            )

    checkpoint.install(name='nvm',
            test_cmd='command -v nvm',
            install_cmd=r'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash'
            )

    checkpoint.install(name='node',
            test_cmd='node --version',
            install_cmd=r'nvm install node'
            )

    checkpoint.install(name='miniconda',
            test_cmd='conda --version',
            install_cmd=r'curl -o https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh | bash'
            )

    checkpoint.install(name='sdkman',
            test_cmd='sdk version',
            install_cmd=r'curl -s "https://get.sdkman.io" | bash'
            )
    
    checkpoint.install(name='texlive',
            test_cmd='latex -version',
            install_cmd={
                'ubuntu': 'sudo apt install texlive-bin',
                'wsl': '@ubuntu',
                'other_linux': None
                }
            )
