# setup-new-machine

A script to handhold me through setting up a new linux (or WSL) machine.

## How to Run
1. Install python 3.8 (e.g. sudo apt install python3.8)
2. Install pyyaml (pip install pyyaml)
3. Run `python3 setup-new-machine.py`

## TODO:
 - Test
 - Add everything else to install:
   - tldr
   - rg
   - autojump
 - Add rest of things to configure:
   - Setup directory structure
   - Setup ssh key
   - Instruction: manually add SSH key to gitlab
   - Clone dotfiles repo
   - Write `dotfiles/zsh_local`
 - Package using pyinstaller
 - Deploy (gitlab CI?)
 - Move specific install scripts into a yaml file
 - Add docker-based automated test
